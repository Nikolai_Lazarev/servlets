package com.epam.servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class RequestListener implements ServletRequestListener {

	private static final Logger logger = Logger.getLogger(RequestListener.class);

	@Override
	public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

	}

	@Override
	public void requestInitialized(ServletRequestEvent servletRequestEvent) {
		System.out.println(servletRequestEvent.toString());
	}
}
