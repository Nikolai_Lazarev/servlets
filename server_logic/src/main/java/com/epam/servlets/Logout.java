package com.epam.servlets;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Logout extends HttpServlet {

	private static final Logger logger = Logger.getLogger(Logout.class);

	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
		httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/authorization");
	}

	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		HttpSession session = httpServletRequest.getSession();
		session.removeAttribute("password");
		session.removeAttribute("login");
		session.invalidate();
		try {
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/authorization");
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
