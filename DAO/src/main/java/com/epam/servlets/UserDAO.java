package com.epam.servlets;


import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements IUserDAO {
	private static Logger logger = Logger.getLogger(UserDAO.class);

	@Override
	public List<User> findAll() {
		ArrayList<User> result = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(manager.getDb(), manager.getUser(), manager.getPassword());
		     PreparedStatement statement = connection.prepareStatement("select * from User");
		     ResultSet resultSet = statement.executeQuery()) {
			while (resultSet.next())
				result.add(new User(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("roleId")));
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	public boolean create(User user) {
		return false;
	}

	@Override
	public User findByLoginAndPassword(String login, String password) {
		User result = null;
		try (Connection connection = DriverManager.getConnection(manager.getDb(), manager.getUser(), manager.getPassword());
		     PreparedStatement statement = connection.prepareStatement("select * from User where password = md5(?) and login = ?")) {
			statement.setString(1, password);
			statement.setString(2, login);
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					result = new User(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("roleId"));
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	public boolean registration(String login, String password) {
		boolean result = false;
			try (Connection connection = DriverManager.getConnection(manager.getDb(), manager.getUser(), manager.getPassword());
			     PreparedStatement statement = connection.prepareStatement("insert into User(roleId, login, password) value(2, ?, md5(?))")) {
				statement.setString(1, login);
				statement.setString(2, password);
				statement.executeUpdate();
				result = true;
			} catch (SQLException e) {
				logger.error(e);
			}
		return result;
	}


	@Override
	public boolean checkEmail(String email) {
		boolean result = false;
		try (Connection connection = DriverManager.getConnection(manager.getDb(), manager.getUser(), manager.getPassword());
		     PreparedStatement statement = connection.prepareStatement("select * from User where login = ?")) {
			statement.setString(1, email);
			statement.executeQuery();
			try (ResultSet resultSet = statement.getResultSet()) {
				if (resultSet.next()) result = true;
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}
}
