package com.epam.servlets;

import java.util.List;

public interface IServletDAO <T extends Entity>{
	List<T> findAll();
	boolean create(T t);
}
