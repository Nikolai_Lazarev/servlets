package com.epam.servlets;

import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class Registration extends HttpServlet {

	private static final Logger logger = Logger.getLogger(Registration.class);

	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		try {
			httpServletResponse.sendRedirect("registration.jsp");
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		String login = httpServletRequest.getParameter("login");
		String password = httpServletRequest.getParameter("password");
		HttpSession session = httpServletRequest.getSession();
		UserDAO test = new UserDAO();
		try {
			if (test.checkEmail(login)) {
				RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/JSP/authorization.jsp");
				httpServletResponse.addCookie(new Cookie("login", login));
				requestDispatcher.include(httpServletRequest, httpServletResponse);
			} else if (test.registration(login, password)) {
				httpServletResponse.addCookie(new Cookie("login", login));
				session.setAttribute("login", login);
				session.setAttribute("password", password);
				session.setAttribute("role", 2);
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/home");
			}
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

}
