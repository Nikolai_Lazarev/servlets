<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.epam.servlets.LocalizationManager"%>
<html>
<head>
 <link rel="stylesheet" href="style.css">
</head>
<body>
<div class = "container">
    <div class = "section">
        <h2><%=LocalizationManager.getInstance().getPropertyByKey("fireworks")%></h2>
        <a href = "authorization.jsp"><%=LocalizationManager.getInstance().getPropertyByKey("authorizationLink")%></a>
        <a href = "registration.jsp"><%=LocalizationManager.getInstance().getPropertyByKey("registrationLink")%></a>
         <jsp:include page = "localization.jsp"/>
     </div>
 </div>
</body>
</html>
