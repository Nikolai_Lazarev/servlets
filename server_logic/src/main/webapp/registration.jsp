<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.epam.servlets.LocalizationManager"%>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <script src="../scripts/checkValidForm.js"></script>
</head>



<body>
<div class="container">
  <form method="POST" action="/registration">
        <h2><%=LocalizationManager.getInstance().getPropertyByKey("titleRegistration")%></h2>
        <input type="text" id = "login" required pattern = "[^@]+@[^@]+\.[a-zA-Z]{2,6}" placeholder = "<%=LocalizationManager.getInstance().getPropertyByKey("login")%>" name = "login"></br>
        <input type="password" autocomplete="off" readonly
                               onfocus="this.removeAttribute('readonly');" autocomplete="off" min = "8" id = "password" required pattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" placeholder="<%=LocalizationManager.getInstance().getPropertyByKey("password")%>" name = "password"></br>
        <input type="password" autocomplete="off" readonly
                               onfocus="this.removeAttribute('readonly');"autocomplete="off" min = "8" id = "confirmPassword" required pattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" placeholder="<%=LocalizationManager.getInstance().getPropertyByKey("password")%>" name = "confirmPassword"></br>
        <input type="submit" value="<%=LocalizationManager.getInstance().getPropertyByKey("sendRegistration")%>"/></br>
        <a href = "authorization.jsp"><%=LocalizationManager.getInstance().getPropertyByKey("authorizationLink")%></a>
    </form>
      <jsp:include page = "localization.jsp"/>
 </div>
</body>
</html>
