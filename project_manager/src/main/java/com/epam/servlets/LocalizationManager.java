package com.epam.servlets;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class LocalizationManager {

	private static LocalizationManager instance = null;
	private static final Logger logger = Logger.getLogger(LocalizationManager.class);
	private Properties properties;
	private static Language language;

	private LocalizationManager() throws IOException {
		String resource;
		properties = new Properties();
		if (language == null) {
			language = Language.RUSS;
		}
		switch (language) {
			case RUSS:
				resource = "/RussianLocalization.properties";
				break;
			case ENG:
				resource = "/EnglishLocalization.properties";
				break;
			default:
				resource = "/RussianLocalization.properties";
		}

		properties.load(getClass().getResourceAsStream(resource));
	}

	public void setLanguage(Language language) {
		this.language = language;
		try {
			instance = new LocalizationManager();
		} catch (IOException e) {
			logger.error(e);
		}
	}


	public static LocalizationManager getInstance() {
		if (instance != null) {
			return instance;
		} else {
			try {
				instance = new LocalizationManager();
			} catch (IOException e) {

			}
			return instance;
		}
	}

	public String getPropertyByKey(String key) {
		return new String(properties.getProperty(key).getBytes(StandardCharsets.ISO_8859_1));
	}

}
