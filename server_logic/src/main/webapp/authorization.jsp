<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.epam.servlets.LocalizationManager"%>
<html>
<head>
    <script src = "/scripts/cookiesAPI.js"></script>
     <link rel="stylesheet" href="style.css">
    <script>
        window.onload = function(){
                   document.getElementById('login').value = getCookie('login').replaceAll("\"", "");
            }
    </script>
</head>
<body>


<div class="container">
  <form i action="/authorization" method="POST">
  <h2><%=LocalizationManager.getInstance().getPropertyByKey("titleAuthorization")%></h2>
        <input type="text" id = "login" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"  placeholder="<%=LocalizationManager.getInstance().getPropertyByKey("login")%>" name = "login"></br>
        <input type="password" autocomplete="off" readonly
                               onfocus="this.removeAttribute('readonly');" required pattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  placeholder="<%=LocalizationManager.getInstance().getPropertyByKey("password")%>" name = "password"></br>
        <input type="submit" value = "<%=LocalizationManager.getInstance().getPropertyByKey("sendAuthorization")%>"></br>
        <a href = "registration.jsp"><%=LocalizationManager.getInstance().getPropertyByKey("registrationLink")%></a>
    </form>
      <jsp:include page = "localization.jsp"/>
    </div>
</body>
</html>
