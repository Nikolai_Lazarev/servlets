<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.epam.servlets.LocalizationManager"%>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <script src="../scripts/checkValidForm.js"></script>
</head>
<body>
<div class = "container">
    <div class = "section">
        <h2><%=LocalizationManager.getInstance().getPropertyByKey("fireworks")%></h2>
        <h2><%=request.getSession().getAttribute("role")%></h2>
        <form action = "/logout" method = "POST">
        <input type = "submit"/ value = "<%=LocalizationManager.getInstance().getPropertyByKey("logout")%>">
        </form>
        <jsp:include page = "localization.jsp"/>
    </div>
</div>
</body>
</html>
