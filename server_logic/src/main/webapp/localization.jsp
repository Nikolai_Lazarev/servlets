<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.epam.servlets.LocalizationManager"%>
<html>
<head>
    <script>
    var sel = document.getElementsByTagName('select')[0];
            function setLanguage(){
                let option = Array.from(sel);
                let lang;
                option.forEach(function(el){
                    if(el.selected){
                       lang = el.value;
                    }
                })
                alert(lang);
    }</script>
</head>
<body>
     <form action="/language">
            <select name="language">
                <option>RUS</option>
                <option>ENG</option>
            </select>
            <input type="submit" onClick="setLanguage()" value="<%=LocalizationManager.getInstance().getPropertyByKey("apply")%>">
        </form>
</body>
</html>
