package com.epam.servlets;

public interface IUserDAO extends IServletDAO<User>{
	PropertiesManager manager = PropertiesManager.getInstance();
	User findByLoginAndPassword(String login, String password);
	boolean registration(String login, String password);
	boolean checkEmail(String email);
}
