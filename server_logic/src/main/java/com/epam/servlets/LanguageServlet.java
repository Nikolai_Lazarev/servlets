package com.epam.servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LanguageServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(LanguageServlet.class);

	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		String lang = httpServletRequest.getParameter("language");
		LocalizationManager manager = LocalizationManager.getInstance();
		switch (lang) {
			case "RUS":
				manager.setLanguage(Language.RUSS);
				break;
			default:
				manager.setLanguage(Language.ENG);
		}
		try {
			httpServletResponse.sendRedirect(httpServletRequest.getHeader("Referer"));
		} catch (IOException e) {
			logger.error(e);
		}

	}

	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
		doGet(httpServletRequest, httpServletResponse);
	}
}
