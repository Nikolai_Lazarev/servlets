package com.epam.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/authorization")
public class Authorization extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("authorization.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		HttpSession session = req.getSession();
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		User user = null;
		if ((user = new UserDAO().findByLoginAndPassword(login, password)) != null) {
			session.setAttribute("login", login);
			session.setAttribute("password", password);
			switch (user.getRole()){
				case 1:
					session.setAttribute("role","admin");
					break;
				case 2:
					session.setAttribute("role", "user");
					break;
				default:
					session.setAttribute("role", "user");
			}
			resp.sendRedirect(req.getContextPath()+"/home");
		} else {
			resp.sendRedirect("authorization.jsp");
		}
	}


}
