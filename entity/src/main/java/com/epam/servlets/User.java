package com.epam.servlets;

public class User extends Entity
{
   private int id;
   private String login;
   private int role;

   public User(int id, String login, int role) {
      this.id = id;
      this.login = login;
      this.role = role;
   }

   public int getId() {
      return id;
   }

   public String getLogin() {
      return login;
   }

   public int getRole() {
      return role;
   }
}
