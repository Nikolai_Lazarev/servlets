package com.epam.servlets;


import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
	private Properties properties;
	private static PropertiesManager instance = null;

	private PropertiesManager() {
		properties = new Properties();
		try {
			properties.load(getClass().getResourceAsStream("/db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static PropertiesManager getInstance(){
		if (null != instance ){
			return instance;
		}
		instance = new PropertiesManager();
		return instance;
	}

	public String getDb() {
		return properties.getProperty("dbconnection");
	}

	public String getUser() {
		return properties.getProperty("user");
	}

	public String getPassword() {
		return properties.getProperty("password");
	}

}
