package com.epam.servlets;


import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class AuthorizationFilter implements Filter {

	private static final Logger logger = Logger.getLogger(AuthorizationFilter.class);

	@Override
	public void init(FilterConfig filterConfig) {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		HttpServletResponse resp = (HttpServletResponse) servletResponse;
		final HttpSession session = req.getSession();

		try {
			if (session != null && session.getAttribute("password") != null && session.getAttribute("login") != null) {
				filterChain.doFilter(servletRequest, servletResponse);
			} else {
				resp.sendRedirect(req.getContextPath() + "/authorization.jsp");
				filterChain.doFilter(servletRequest, servletResponse);
			}
		} catch (IOException e) {
			logger.error(e);
		} catch (ServletException e) {
			logger.error(e);
		}
	}

	@Override
	public void destroy() {

	}
}
